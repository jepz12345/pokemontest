<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PokemonModel;

class ApiController extends Controller
{
    

    public function usernames()
    {
        $usernames = [
            [
                'username' => 'aaa@gmail.com',
                'pw' => 'pw2020'
            ],
            [
                'username' => 'bbb@gmail.com',
                'pw' => 'pw2019'
            ],
        ];

        return $usernames;
    }


    public function login(Request $request)
    {
        $user = $request->get('username');
        $pw = $request->get('pw');

        $auth = false;

        foreach($this->usernames() as $u)
        {
            if ($u['username'] == $user && $u['pw'] == $pw)
                $auth = true;
        }

        if($auth)
        {
            $request->session()->put('authenticated', time());
            $request->session()->put('username', $user);
            return "Hello {$user}";
        }
        else
        {
            $request->session()->flush();
            return "unauthenticated";
        }
    }


    public function logout(Request $request)
    {
        $request->session()->flush();
        return 'logged out';
    }

    public function test(Request $request)
    {
        if(!$this->checkAuth($request))
            return redirect('authFail');

        $username = $request->session()->get('username');
        return "Hello {$username}";
    }

    public function checkAuth($request)
    {
        if(empty($request->session()->get('authenticated')))
            return false;
        else
            return true;
    }

    public function getPokemonList(Request $request)
    {
        $pokemonModel = new PokemonModel();
        $data = $pokemonModel->getPokemonList();

        if(!empty($request->get('name')))
        {
            $data = collect($data->where('name', $request->get('name')));
        }

        if(!empty($request->get('limit')))
        {
            $data = collect($data->take($request->get('limit')));
        }

        if(!empty($request->get('sort')))
        {
            $data = collect($data->sortBy($request->get('sort')));
        }

        return $data;
    }
}
