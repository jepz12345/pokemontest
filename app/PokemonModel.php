<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonModel extends Model
{
    private $jsonFile;

    public function __construct()
    {
        $this->jsonFile = storage_path() . '\pokemon.json';
    }

    public function getPokemonList()
    {
        $data = collect(json_decode(file_get_contents($this->jsonFile)));
        return $data;
    }
    
}
