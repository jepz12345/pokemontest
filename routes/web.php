<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'ApiController@test');
Route::get('/login', 'ApiController@login');
Route::get('/logout', 'ApiController@logout');

Route::get('/getPokemonList', 'ApiController@getPokemonList');

Route::get('/authFail', function () {
    return 'unauthorized';
});

Route::get('/', function () {
    return view('welcome');
});
